package cat.itb.controlStock;
import cat.itb.stock.Comanda;
import cat.itb.stock.Producte;

import java.text.ParseException;


public class ProvaLlibreriaJava1 {
    public static void main(String[] args) throws ParseException {
        Producte producte = new Producte(10,"Dabber Sur Femme 2020", 25,20, 50);
        Comanda comanda = new Comanda();
        comanda.setProducte(producte);
        producte.addPropertyChangeListener(comanda);
        producte.setStockactual(18);

    }
}
